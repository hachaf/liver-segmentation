class CSVBuilder:
    def __init__(self):
        self.lines = []
        self.header = []
        self.header_initiated = False

    def add_row(self, json, acc):
        if not self.header:
            for key in json:
                self.header.append(key)
            self.header.append('acc')

        line = []
        for key in self.header:
            if key == 'acc':
                line.append(str(acc))
            else:
                line.append(str(json[key]))

        self.lines.append(line)

    def save_to_file(self, file_path):
        f = open(file_path, 'w')

        # Write header
        f.write(';'.join(self.header))
        f.write('\n')

        # Write lines
        for line in self.lines:
            f.write(';'.join(line))
            f.write('\n')

        f.close()
