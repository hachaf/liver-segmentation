from matplotlib import pyplot as plt


def plot_loss_train(loss_history):
    plt.plot(loss_history)
    plt.title('Model loss (train dataset)')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.show()


def plot_accuracy_train(accuracy_history):
    plt.plot(accuracy_history)
    plt.title('Model accuracy (train dataset)')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.show()


def plot_precision_train(precision_history):
    plt.plot(precision_history)
    plt.title('Model precision (train dataset)')
    plt.ylabel('Precision')
    plt.xlabel('Epoch')
    plt.show()


def plot_recall_train(recall_history):
    plt.plot(recall_history)
    plt.title('Model recall (train dataset)')
    plt.ylabel('Recall')
    plt.xlabel('Epoch')
    plt.show()


def plot_loss_validation(val_loss_history):
    plt.plot(val_loss_history)
    plt.title('Model loss (validation dataset)')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.show()


def plot_accuracy_validation(val_accuracy_history):
    plt.plot(val_accuracy_history)
    plt.title('Model accuracy (validation dataset)')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.show()


def plot_precision_validation(val_precision_history):
    plt.plot(val_precision_history)
    plt.title('Model precision (validation dataset)')
    plt.ylabel('Precision')
    plt.xlabel('Epoch')
    plt.show()


def plot_recall_validation(val_recall_history):
    plt.plot(val_recall_history)
    plt.title('Model recall (validation dataset)')
    plt.ylabel('Recall')
    plt.xlabel('Epoch')
    plt.show()


def plot_loss(loss_history, val_loss_history):
    plt.plot(loss_history)
    plt.plot(val_loss_history)
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['train dataset', 'validation dataset'])
    plt.show()


def plot_accuracy(accuracy_history, val_accuracy_history):
    plt.plot(accuracy_history)
    plt.plot(val_accuracy_history)
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['train dataset', 'validation dataset'])
    plt.show()


def plot_precision(precision_history, val_precision_history):
    plt.plot(precision_history)
    plt.plot(val_precision_history)
    plt.title('Model precision')
    plt.ylabel('Precision')
    plt.xlabel('Epoch')
    plt.legend(['train dataset', 'validation dataset'])
    plt.show()


def plot_recall(recall_history, val_recall_history):
    plt.plot(recall_history)
    plt.plot(val_recall_history)
    plt.title('Model recall')
    plt.ylabel('Recall')
    plt.xlabel('Epoch')
    plt.legend(['train dataset', 'validation dataset'])
    plt.show()


def plot_slices(img3d, file_path):
    for i in range(0, img3d.shape[0]):
        plt.imsave(f'{file_path}_{i}.png', img3d[i])
