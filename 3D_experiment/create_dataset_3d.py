import sys
import os
import io3d
import numpy as np
import h5py
from bodynavigation.body_navigation import BodyNavigation
from sklearn.utils import class_weight

from configuration.experiment_configuration import load_configuration
from utils.plot2d import plot_slices


def compute_extremes(config):
    extremes = {
        'intensity_min': float('Inf'),
        'intensity_max': float('-Inf'),
        'sagittal_min': float('Inf'),
        'sagittal_max': float('-Inf'),
        'axial_min': float('Inf'),
        'axial_max': float('-Inf'),
        'coronal_min': float('Inf'),
        'coronal_max': float('-Inf'),
        'body_surf_min': float('Inf'),
        'body_surf_max': float('-Inf')
    }
    min_intensity = float('Inf')
    max_intensity = float('-Inf')
    min_sagittal = float('Inf')
    max_sagittal = float('-Inf')
    min_axial = float('Inf')
    max_axial = float('-Inf')
    min_coronal = float('Inf')
    max_coronal = float('-Inf')
    min_body_surf = float('Inf')
    max_body_surf = float('-Inf')

    for dataset_item in config["train_dataset_items"]:
        dataset_name = dataset_item["data_set"]
        dataset_index = dataset_item["id"]
        print(f'Processing extremes: dataset={dataset_name}, index={dataset_index}')
        medical_data = io3d.read_dataset(dataset_name, 'data3d', dataset_index, orientation_axcodes='SPL')
        img_data = medical_data["data3d"]
        bn = BodyNavigation(medical_data["data3d"], voxelsize_mm=medical_data["voxelsize_mm"])
        sagittal_data = bn.dist_to_sagittal()
        axial_data = bn.dist_to_axial()
        coronal_data = bn.dist_to_coronal()
        body_surf_data = bn.dist_to_surface()

        extremes['intensity_min'] = min(img_data.min(), min_intensity)
        extremes['intensity_max'] = max(img_data.max(), max_intensity)
        extremes['sagittal_min'] = min(sagittal_data.min(), min_sagittal)
        extremes['sagittal_max'] = max(sagittal_data.max(), max_sagittal)
        extremes['axial_min'] = min(axial_data.min(), min_axial)
        extremes['axial_max'] = max(axial_data.max(), max_axial)
        extremes['coronal_min'] = min(coronal_data.min(), min_coronal)
        extremes['coronal_max'] = max(coronal_data.max(), max_coronal)
        extremes['body_surf_min'] = min(body_surf_data.min(), min_body_surf)
        extremes['body_surf_max'] = max(body_surf_data.max(), max_body_surf)

    return extremes


def load_dataset(dataset_items, cube_size, offset_x, offset_y, offset_z, plot_dir):
    # Prepare result lists
    dataset_img_list = []
    dataset_mask_list = []
    dataset_sagittal_list = []
    dataset_axial_list = []
    dataset_coronal_list = []
    dataset_body_surf_list = []

    plot_dir = f'{plot_dir}/img/src'

    if not os.path.exists(plot_dir):
        os.makedirs(plot_dir)

    index = 0
    for dataset_item in dataset_items:
        dataset_index = dataset_item['id']
        dataset_name = dataset_item['data_set']

        print(f'Loading train dataset {index} from image {dataset_index}')

        # Retrieve data
        medical_data = io3d.read_dataset(dataset_name, 'data3d', dataset_index, orientation_axcodes='SPL')
        img_data = medical_data["data3d"]
        mask_data = np.zeros(img_data.shape)
        bn = BodyNavigation(medical_data["data3d"], voxelsize_mm=medical_data["voxelsize_mm"])
        sagittal_data = bn.dist_to_sagittal()
        axial_data = bn.dist_to_axial()
        coronal_data = bn.dist_to_coronal()
        body_surf_data = bn.dist_to_surface()

        # Assemble final mask
        for mask in configuration["masks"]:
            current_mask_data = io3d.read_dataset(dataset_name, mask, dataset_index, orientation_axcodes='SPL')[
                "data3d"]
            mask_data = np.maximum(mask_data, current_mask_data)

        plot_slices(img_data, f'{plot_dir}/intensity_{dataset_item["data_set"]}_{dataset_item["id"]}')
        plot_slices(sagittal_data, f'{plot_dir}/sagittal_{dataset_item["data_set"]}_{dataset_item["id"]}')
        plot_slices(axial_data, f'{plot_dir}/axial_{dataset_item["data_set"]}_{dataset_item["id"]}')
        plot_slices(coronal_data, f'{plot_dir}/coronal_{dataset_item["data_set"]}_{dataset_item["id"]}')
        plot_slices(body_surf_data, f'{plot_dir}/body_surf_{dataset_item["data_set"]}_{dataset_item["id"]}')
        plot_slices(mask_data, f'{plot_dir}/mask_{dataset_item["data_set"]}_{dataset_item["id"]}')

        # TODO Resize 3D image to 32*n

        # Cubes
        i = offset_x
        while i <= img_data.shape[0] - cube_size:
            j = offset_y
            while j <= img_data.shape[1] - cube_size:
                k = offset_z
                while k <= img_data.shape[2] - cube_size:
                    dataset_img_list.append(
                        img_data[i:i + cube_size, j:j + cube_size, k:k + cube_size].astype('float32'))
                    dataset_sagittal_list.append(
                        sagittal_data[i:i + cube_size, j:j + cube_size, k:k + cube_size].astype('float32'))
                    dataset_axial_list.append(
                        axial_data[i:i + cube_size, j:j + cube_size, k:k + cube_size].astype('float32'))
                    dataset_coronal_list.append(
                        coronal_data[i:i + cube_size, j:j + cube_size, k:k + cube_size].astype('float32'))
                    dataset_body_surf_list.append(
                        body_surf_data[i:i + cube_size, j:j + cube_size, k:k + cube_size].astype('float32'))
                    dataset_mask_list.append(
                        mask_data[i:i + cube_size, j:j + cube_size, k:k + cube_size].astype('float32'))
                    k += cube_size
                j += cube_size
            i += cube_size

        index += 1

    # Convert lists to Numpy arrays
    dataset = {'img': np.array(dataset_img_list).astype('float32'),
               'mask': (np.array(dataset_mask_list) > 0).astype('float32'),
               'sagittal': np.array(dataset_sagittal_list).astype('float32'),
               'axial': np.array(dataset_axial_list).astype('float32'),
               'coronal': np.array(dataset_coronal_list).astype('float32'),
               'body_surf': np.array(dataset_body_surf_list).astype('float32')}
    dataset['shape'] = [dataset['img'].shape[0], dataset['img'].shape[1], dataset['img'].shape[2],
                        dataset['img'].shape[3]]
    return dataset


def normalize_dataset(dataset):
    # Normalize
    dataset['img'] = (dataset['img'] - extremes['intensity_min']) / (
                extremes['intensity_max'] - extremes['intensity_min'])
    dataset['sagittal'] = (dataset['sagittal'] - extremes['sagittal_min']) / (
                extremes['sagittal_max'] - extremes['sagittal_min'])
    dataset['axial'] = (dataset['axial'] - extremes['axial_min']) / (
                extremes['axial_max'] - extremes['axial_min'])
    dataset['coronal'] = (dataset['coronal'] - extremes['coronal_min']) / (
                extremes['coronal_max'] - extremes['coronal_min'])
    dataset['body_surf'] = (dataset['body_surf'] - extremes['body_surf_min']) / (
                extremes['body_surf_max'] - extremes['body_surf_min'])


def save_dataset(dataset, config, filename):
    h5f = h5py.File(filename, 'w')
    h5f.create_dataset('x', data=dataset['img'], compression=config['dataset_compression'])
    h5f.create_dataset('sagittal', data=dataset['sagittal'], compression=config['dataset_compression'])
    h5f.create_dataset('axial', data=dataset['axial'], compression=config['dataset_compression'])
    h5f.create_dataset('coronal', data=dataset['coronal'], compression=config['dataset_compression'])
    h5f.create_dataset('body_surf', data=dataset['body_surf'], compression=config['dataset_compression'])
    h5f.create_dataset('y', data=dataset['mask'], compression=config['dataset_compression'])
    h5f.close()


def compute_class_weights(config):
    dataset_mask_list = np.array([], dtype=float)
    for dataset_item in config['train_dataset_items']:
        dataset_name = dataset_item["data_set"]
        dataset_index = dataset_item["id"]
        medical_data = io3d.read_dataset(dataset_name, 'data3d', dataset_index, orientation_axcodes='SPL')
        img_data = medical_data["data3d"]
        mask_data = np.zeros(img_data.shape)

        for mask in configuration["masks"]:
            current_mask_data = io3d.read_dataset(dataset_name, mask, dataset_index, orientation_axcodes='SPL')[
                "data3d"]
            mask_data = np.maximum(mask_data, current_mask_data)

        dataset_mask_list = np.concatenate((dataset_mask_list, (mask_data > 0).astype('float32').flatten()))

    cl_weights = class_weight.compute_class_weight('balanced', np.unique(dataset_mask_list), dataset_mask_list)
    return cl_weights


def save_class_weights(config, cl_weights):
    h5f = h5py.File(config['class_weights_file_path'], 'w')
    h5f.create_dataset('weights', data=cl_weights)
    h5f.close()


# Load experiment configuration
configuration = load_configuration(sys.argv)
cube_size = configuration["cube_size"]

# Create data folder if it does not exist
if not os.path.exists(f'{configuration["data_dir_path"]}'):
    os.makedirs(f'{configuration["data_dir_path"]}')

# Compute normalization constants
extremes = compute_extremes(configuration)

# Class weights
print('Computing class weights')
cl_weights = compute_class_weights(configuration)
save_class_weights(configuration, cl_weights)
print('Class weights computed')

# Load train dataset
for dataset_item in configuration["train_dataset_items"]:
    dataset_name = dataset_item["data_set"]
    dataset_index = dataset_item["id"]
    offsets = []
    offset_index = 0
    for offset_x in range(0, cube_size, configuration["offset_step"]):
        for offset_y in range(0, cube_size, configuration["offset_step"]):
            for offset_z in range(0, cube_size, configuration["offset_step"]):
                dataset_path = f'{configuration["train_dataset_prefix"]}{dataset_index}_{offset_index}.h5'
                if os.path.exists(dataset_path):
                    offset_index += 1
                    continue

                train_dataset = load_dataset([dataset_item], cube_size, offset_x, offset_y, offset_z, configuration['data_dir_path'])
                normalize_dataset(train_dataset)
                save_dataset(train_dataset, configuration, dataset_path)

                offset_index += 1

# Validation dataset
if not os.path.exists(configuration["validation_dataset_file_path"]):
    validation_dataset = load_dataset(configuration["validation_dataset_items"], cube_size, 0, 0, 0, configuration['data_dir_path'])
    normalize_dataset(validation_dataset)
    save_dataset(validation_dataset, configuration, configuration["validation_dataset_file_path"])

# Test dataset
if os.path.exists(configuration["test_dataset_file_path"]):
    test_dataset = load_dataset(configuration["test_dataset_items"], cube_size, 0, 0, 0, configuration['data_dir_path'])
    normalize_dataset(test_dataset)
    save_dataset(test_dataset, configuration, configuration["test_dataset_file_path"])
