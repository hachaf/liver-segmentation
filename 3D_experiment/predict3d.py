import os
import sys
from tensorflow.keras.optimizers import Adam
import io3d
import numpy as np
import time
from matplotlib import pyplot as plt
import imma.volumetry_evaluation
import scipy.signal
import scipy.ndimage
import scipy.ndimage.morphology
import sklearn.metrics
from skimage.segmentation import mark_boundaries
import skimage.io

from configuration.experiment_configuration import load_configuration
from gpu.gpu_configuration import enable_gpu_computation
from datasets.datasets_utils import get_data_shape
from datasets.datasets_utils import load_class_weights
from datasets.datasets_utils import load_dataset
from neural.unet3d import create_unet
from neural.loss_functions import weighted_binary_crossentropy
from postprocess.volumes_filtering import filter_largest_area
from postprocess.volumes_filtering import morphology_postprocess
from utils.csv_builder import CSVBuilder
from normalization.grid_size_normalizer import resize_grid


def get_dataset_dimensions(dataset_items, cube_size, offset_x, offset_y, offset_z):
    dimensions_list = []
    voxel_sizes = []

    for dataset_item in dataset_items:

        dataset_index = dataset_item['id']
        dataset_name = dataset_item['data_set']

        # Retrieve data
        medical_data = io3d.read_dataset(dataset_name, 'data3d', dataset_index, orientation_axcodes='SPL')
        img_data = medical_data["data3d"]

        # TODO Resize 3D image to 32*n

        count_x = 0
        count_y = 0
        count_z = 0

        i = offset_x
        while i <= img_data.shape[0] - cube_size:
            count_x += 1
            i += cube_size

        j = offset_y
        while j <= img_data.shape[1] - cube_size:
            count_y += 1
            j += cube_size

        k = offset_z
        while k <= img_data.shape[1] - cube_size:
            count_z += 1
            k += cube_size

        dimensions_list.append(np.array([count_x, count_y, count_z]))
        voxel_sizes.append(medical_data['voxelsize_mm'])

    return np.array(dimensions_list), np.array(voxel_sizes)


def calculate_accuracy(y_true, y_pred):
    return sklearn.metrics.accuracy_score(y_true.flatten(), y_pred.flatten())


# Load configuration
configuration = load_configuration(sys.argv)

# Enable GPU
enable_gpu_computation()

# Load dimensions of 3D images
cube_size = configuration["cube_size"]
dimensions, voxel_sizes = get_dataset_dimensions(configuration["validation_dataset_items"], cube_size, 0, 0, 0)

# Prepare CSV results builder
csv_builder = CSVBuilder()

# Create folder for images if it does not exist
if not os.path.exists(f'{configuration["data_dir_path"]}/img'):
    os.makedirs(f'{configuration["data_dir_path"]}/img')

# Create model and load weights
data_shape = get_data_shape(configuration)
cl_weights = load_class_weights(configuration)
model = create_unet(input_size=data_shape)
model.compile(optimizer=Adam(lr=1e-4), loss=weighted_binary_crossentropy(cl_weights[0], cl_weights[1]),
              metrics=['accuracy'])
model.load_weights(configuration['last_weights_file'])

# Load test data
test_x, test_y = load_dataset(configuration['validation_dataset_file_path'], configuration)

# Loop through each image
cube_index = 0
for n in range(0, dimensions.shape[0]):
    dim = dimensions[n]
    im_size = [dim[0] * cube_size, dim[1] * cube_size, dim[2] * cube_size]
    x = np.zeros(im_size)
    y_true = np.zeros(im_size)
    y_pred = np.zeros(im_size)

    im_cubes = test_x[cube_index:cube_index+dim[0]*dim[1]*dim[2]]
    cubes_prediction = model.predict(im_cubes, batch_size=1, verbose=1)
    prev_cube_index = cube_index

    for i in range(dim[0]):
        for j in range(dim[1]):
            for k in range(dim[2]):
                i_from = i * cube_size
                i_to = (i+1) * cube_size
                j_from = j * cube_size
                j_to = (j + 1) * cube_size
                k_from = k * cube_size
                k_to = (k + 1) * cube_size

                cube_intensity = test_x[cube_index][::, ::, ::, 0]
                x[i_from:i_to, j_from:j_to, k_from:k_to] = cube_intensity
                y_true[i_from:i_to, j_from:j_to, k_from:k_to] = test_y[cube_index][::, ::, ::, 0]

                # cube_x = test_x[cube_index]
                # cube_x = cube_x.reshape([1, test_x.shape[1], test_x.shape[2], test_x.shape[3], test_x.shape[4]])

                # cube_y = model.predict(x=cube_x)
                y_pred[i_from:i_to, j_from:j_to, k_from:k_to] = cubes_prediction[cube_index - prev_cube_index, ::, ::, ::, 0]

                cube_index += 1

    # Threshold
    y_pred_th = y_pred > 0.5
    y_pred_th = y_pred_th.astype('float32')

    # Postprocess
    y_pred_post = y_pred_th
    if configuration['filter_largest_area']:
        print('Filtering largest area')
        start = time.time()
        y_pred_post = filter_largest_area(y_pred_post)
        end = time.time()
        print(f'Largest area filtered. Time elapsed: {end - start} s')

    if configuration['filter_gaussian'] is not None:
        print('Gaussian filtering started')
        y_pred_post = scipy.ndimage.gaussian_filter(y_pred_post, configuration['filter_gaussian'])
        y_pred_post = (y_pred_post > 0.5).astype('float32')
        print('Gaussian filtering done')

    if configuration['morphology_operations'] is not None:
        for operation_code in configuration['morphology_operations']:
            y_pred_post = morphology_postprocess(y_pred_post, operation_code)

    # Metrics computation
    evaluation, diff = imma.volumetry_evaluation.compare_volumes_sliver(y_true, y_pred_post, voxelsize_mm=voxel_sizes[n],
                                                                        return_diff=True)
    print(evaluation)
    acc = calculate_accuracy(y_true, y_pred_post)
    print(f'Accuracy: {acc}')
    csv_builder.add_row(evaluation, acc)

    # Slices image export
    for i in range(0, x.shape[0]):
        f, axarr = plt.subplots(3, 2)
        axarr[0, 0].imshow(x[i].astype('float32'))
        axarr[0, 0].set_title('Intensity')
        axarr[0, 0].axis('off')

        axarr[0, 1].imshow(y_true[i].astype('float32'))
        axarr[0, 1].set_title('True mask')
        axarr[0, 1].axis('off')

        axarr[1, 0].imshow(y_pred[i].astype('float32'))
        axarr[1, 0].set_title('Prediction')
        axarr[1, 0].axis('off')

        axarr[1, 1].imshow(y_pred_th[i].astype('float32'))
        axarr[1, 1].set_title('Threshold')
        axarr[1, 1].axis('off')

        axarr[2, 0].imshow(y_pred_post[i].astype('float32'))
        axarr[2, 0].set_title('Post-processed')
        axarr[2, 0].axis('off')

        axarr[2, 1].imshow(diff[i])
        axarr[2, 1].set_title('Difference')
        axarr[2, 1].axis('off')
        f.savefig(f'{configuration["data_dir_path"]}/img/slice_{n}_{i}.png')
        plt.close(f)

        skimage.io.imsave(f'{configuration["data_dir_path"]}/img/boundary_pred_{n}_{i}.png',
                          mark_boundaries(x[i].astype('float32'), y_pred_post[i].astype('uint8')))

        skimage.io.imsave(f'{configuration["data_dir_path"]}/img/boundary_true_{n}_{i}.png',
                          mark_boundaries(x[i].astype('float32'), y_true[i].astype('uint8')))

csv_builder.save_to_file(configuration['csv_results_path'])
