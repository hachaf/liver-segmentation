import sys
import h5py
from configuration.experiment_configuration import load_configuration
from utils.plot2d import plot_loss_train, plot_accuracy_train, plot_precision_train, plot_recall_train, \
    plot_loss_validation, plot_accuracy_validation, plot_precision_validation, plot_recall_validation, plot_loss, \
    plot_accuracy, plot_precision, plot_recall

# Load configuration
configuration = load_configuration(sys.argv)

# Load previous loss and accuracy
h5f_history = h5py.File(configuration["history_file_path"], 'r')
loss_history = h5f_history['loss'][:]
accuracy_history = h5f_history['accuracy'][:]
precision_history = h5f_history['precision'][:]
recall_history = h5f_history['recall'][:]
val_loss_history = h5f_history['val_loss'][:]
val_accuracy_history = h5f_history['val_accuracy'][:]
val_precision_history = h5f_history['val_precision'][:]
val_recall_history = h5f_history['val_recall'][:]
h5f_history.close()

plot_loss_train(loss_history)
plot_accuracy_train(accuracy_history)
plot_precision_train(precision_history)
plot_recall_train(recall_history)

plot_loss_validation(val_loss_history)
plot_accuracy_validation(val_accuracy_history)
plot_precision_validation(val_precision_history)
plot_recall_validation(val_recall_history)

plot_loss(loss_history, val_loss_history)
plot_accuracy(accuracy_history, val_accuracy_history)
plot_precision(precision_history, val_precision_history)
plot_recall(recall_history, val_recall_history)


print(f'Loss: \n{loss_history}')
print(f'Accuracy: \n{accuracy_history}')
print(f'Precision: \n{precision_history}')
print(f'Recall: \n{recall_history}')
print(f'Loss (validation): \n{val_loss_history}')
print(f'Accuracy (validation): \n{val_accuracy_history}')
print(f'Precision (validation): \n{val_precision_history}')
print(f'Recall (validation): \n{val_recall_history}')
