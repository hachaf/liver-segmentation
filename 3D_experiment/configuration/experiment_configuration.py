import json


def load_configuration(argv):
    if len(argv) < 2:
        raise Exception('Missing experiment configuration file path')

    f = open(argv[1])
    settings = json.load(f)
    f.close()
    return settings
