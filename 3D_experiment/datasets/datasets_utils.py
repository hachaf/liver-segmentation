import h5py
import numpy as np


def load_dataset(file_path, config):
    h5f = h5py.File(file_path, 'r')
    x = h5f['x'][:]
    sagittal = h5f['sagittal'][:]
    axial = h5f['axial'][:]
    coronal = h5f['coronal'][:]
    body_surf = h5f['body_surf'][:]
    y = h5f['y'][:]
    h5f.close()

    features_count = 5 if config['use_bodynavigation'] else 1

    train_x = np.zeros([x.shape[0], x.shape[1], x.shape[2], x.shape[3], features_count], dtype='float32')
    train_y = np.zeros([x.shape[0], x.shape[1], x.shape[2], x.shape[3], 1], dtype='float32')

    dest = 0
    train_x[:, :, :, :, dest] = x
    dest += 1
    if config['use_bodynavigation']:
        train_x[:, :, :, :, dest] = sagittal
        dest += 1
        train_x[:, :, :, :, dest] = axial
        dest += 1
        train_x[:, :, :, :, dest] = coronal
        dest += 1
        train_x[:, :, :, :, dest] = body_surf
        dest += 1
    train_y[:, :, :, :, 0] = y

    return train_x, train_y


def load_class_weights(config):
    h5f = h5py.File(config['class_weights_file_path'], 'r')
    weights = h5f['weights'][:]
    h5f.close()
    return weights


def get_data_shape(config):
    cube_size = config["cube_size"]
    features_count = 5 if config['use_bodynavigation'] else 1
    return [cube_size, cube_size, cube_size, features_count]
