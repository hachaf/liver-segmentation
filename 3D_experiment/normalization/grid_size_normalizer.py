from scipy.ndimage import zoom


def resize_grid(x, shape):
    is_identity = True
    for i in range(0, len(shape)):
        if shape[i] != x.shape[i]:
            is_identity = False
            break

    if is_identity:
        return x

    zoom_factor = []
    for i in range(0, len(shape)):
        zoom_factor.append(shape[i] / x.shape[i])

    return zoom(x, zoom_factor)
