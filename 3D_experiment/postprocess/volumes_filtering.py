import skimage.io
import skimage.measure
import scipy.ndimage.morphology


def filter_largest_area(mask):
    labels = skimage.measure.label(mask, background=0)
    properties = skimage.measure.regionprops(labels)
    max_area = -1
    max_area_index = -1
    areas = [prop.area for prop in properties]
    index = 0
    for area in areas:
        if area > max_area:
            max_area = area
            max_area_index = index
        index += 1

    result = labels == max_area_index + 1
    return result


def morphology_postprocess(mask, operation_code):
    if operation_code == 'D':
        mask = scipy.ndimage.morphology.binary_dilation(mask)
    if operation_code == 'E':
        mask = scipy.ndimage.morphology.binary_erosion(mask)
    if operation_code == 'C':
        mask = scipy.ndimage.morphology.binary_closing(mask)
    if operation_code == 'O':
        mask = scipy.ndimage.morphology.binary_opening(mask)
    if operation_code == 'F':
        mask = scipy.ndimage.morphology.binary_fill_holes(mask)

    return mask
