import os
import sys
import gc
from random import random, Random

import h5py
import numpy as np
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.metrics import Accuracy, Precision, Recall

from configuration.experiment_configuration import load_configuration
from gpu.gpu_configuration import enable_gpu_computation
from neural.loss_functions import weighted_binary_crossentropy
from neural.unet3d import create_unet
from datasets.datasets_utils import get_data_shape
from datasets.datasets_utils import load_class_weights
from datasets.datasets_utils import load_dataset
from utils.plot2d import plot_loss, plot_accuracy, plot_loss_validation, plot_accuracy_validation

# Load configuration
configuration = load_configuration(sys.argv)

# Enable GPU
enable_gpu_computation()

# Create model and load weights
data_shape = get_data_shape(configuration)
cl_weights = load_class_weights(configuration)
model = create_unet(input_size=data_shape)
model.compile(optimizer=Adam(lr=1e-4), loss=weighted_binary_crossentropy(cl_weights[0], cl_weights[1]),
              metrics=[Accuracy(), Precision(), Recall()])

# Load weights
if configuration['continue'] and os.path.exists(configuration["last_weights_file"]):
    model.load_weights(configuration['last_weights_file'])

# Prepare history lists
train_loss_list = []
train_acc_list = []
train_pre_list = []
train_rec_list = []

validation_loss_list = []
validation_acc_list = []
validation_pre_list = []
validation_rec_list = []

# Load test data
validation_x, validation_y = load_dataset(configuration['validation_dataset_file_path'], configuration)

prev_epochs_num = 0
if configuration['continue'] and os.path.exists(f'{configuration["data_dir_path"]}/last_epoch_done.txt'):
    prev_epoch_file = open(f'{configuration["data_dir_path"]}/last_epoch_done.txt', 'r')
    prev_epochs_num = int(prev_epoch_file.readline()) + 1
    prev_epoch_file.close()

# Epochs loop
for i in range(0 + prev_epochs_num, configuration['num_of_epochs'] + prev_epochs_num):
    print(f"Epoch: {i}")

    # Free memory
    gc.collect()

    # Get dataset files names
    dataset_files = os.listdir(configuration['data_dir_path'])
    dataset_files = [f for f in dataset_files if f.startswith(configuration['train_dataset_prefix'].split('/')[-1])]

    # Shuffle dataset files
    Random(1).shuffle(dataset_files)

    # For each dataset
    dataset_index = 0
    skip_files = 0
    for dataset_file in dataset_files:
        current_weights_path = configuration["weights_file_path"].replace('%0', str(i)).replace('%1', str(dataset_index))
        if configuration['continue'] and os.path.exists(current_weights_path):
            dataset_index += 1
            continue

        # Load dataset
        train_x, train_y = load_dataset(f"{configuration['data_dir_path']}\\{dataset_file}", configuration)
        print('Fit: ')
        history = model.fit(x=train_x, y=train_y, batch_size=configuration['batch_size'], verbose=1, epochs=1)

        # Save current weights
        model.save_weights(current_weights_path)
        model.save_weights(configuration['last_weights_file'])

        if dataset_file == dataset_files[-1]:
            print('Evaluate: ')
            eval_history = model.evaluate(x=validation_x, y=validation_y, batch_size=configuration['batch_size'], verbose=1)
            train_loss_list.append(history.history['loss'][-1])
            train_acc_list.append(history.history['accuracy'][-1])
            train_pre_list.append(history.history['precision'][-1])
            train_rec_list.append(history.history['recall'][-1])
            validation_loss_list.append(eval_history[0])
            validation_acc_list.append(eval_history[1])
            validation_pre_list.append(eval_history[2])
            validation_rec_list.append(eval_history[3])

            # Transform history
            loss_history = np.array(train_loss_list)
            accuracy_history = np.array(train_acc_list)
            pre_history = np.array(train_pre_list)
            rec_history = np.array(train_rec_list)
            val_loss_history = np.array(validation_loss_list)
            val_accuracy_history = np.array(validation_acc_list)
            val_pre_history = np.array(validation_pre_list)
            val_rec_history = np.array(validation_rec_list)

            if configuration['continue'] and os.path.exists(configuration["history_file_path"]):
                # Load previous loss and accuracy
                h5f_history = h5py.File(configuration["history_file_path"], 'r')
                prev_loss_history = h5f_history['loss'][:]
                prev_accuracy_history = h5f_history['accuracy'][:]
                prev_pre_history = h5f_history['precision'][:]
                prev_rec_history = h5f_history['recall'][:]
                prev_val_loss_history = h5f_history['val_loss'][:]
                prev_val_accuracy_history = h5f_history['val_accuracy'][:]
                prev_val_pre_history = h5f_history['val_precision'][:]
                prev_val_rec_history = h5f_history['val_recall'][:]
                h5f_history.close()

                # Concat loss and accuracy
                loss_history = np.concatenate((prev_loss_history, loss_history))
                accuracy_history = np.concatenate((prev_accuracy_history, accuracy_history))
                pre_history = np.concatenate((prev_pre_history, pre_history))
                rec_history = np.concatenate((prev_rec_history, rec_history))
                val_loss_history = np.concatenate((prev_val_loss_history, val_loss_history))
                val_accuracy_history = np.concatenate((prev_val_accuracy_history, val_accuracy_history))
                val_pre_history = np.concatenate((prev_val_pre_history, val_pre_history))
                val_rec_history = np.concatenate((prev_val_rec_history, val_rec_history))

            # Write history to H5
            h5f_history = h5py.File(configuration["history_file_path"], 'w')
            h5f_history.create_dataset('loss', data=loss_history)
            h5f_history.create_dataset('accuracy', data=accuracy_history)
            h5f_history.create_dataset('precision', data=pre_history)
            h5f_history.create_dataset('recall', data=rec_history)
            h5f_history.create_dataset('val_loss', data=val_loss_history)
            h5f_history.create_dataset('val_accuracy', data=val_accuracy_history)
            h5f_history.create_dataset('val_precision', data=val_pre_history)
            h5f_history.create_dataset('val_recall', data=val_rec_history)
            h5f_history.close()

            train_loss_list = []
            train_acc_list = []
            train_pre_list = []
            train_rec_list = []
            validation_loss_list = []
            validation_acc_list = []
            validation_pre_list = []
            validation_rec_list = []

        dataset_index += 1

    prev_epoch_file = open(f'{configuration["data_dir_path"]}/last_epoch_done.txt', 'w')
    prev_epoch_file.write(str(i))
    prev_epoch_file.close()
