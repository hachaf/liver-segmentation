from matplotlib import pyplot as plt
import tensorflow as tf
import numpy as np


def plot_loss(loss_history):
    plt.plot(loss_history)
    plt.title('Model loss (train dataset)')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.show()


def plot_accuracy(accuracy_history):
    plt.plot(accuracy_history)
    plt.title('Model accuracy (train dataset)')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.show()


def plot_loss_validation(val_loss_history):
    plt.plot(val_loss_history)
    plt.title('Model loss (validation dataset)')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.show()


def plot_accuracy_validation(val_accuracy_history):
    plt.plot(val_accuracy_history)
    plt.title('Model accuracy (validation dataset)')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.show()


def display(display_list):
    # plt.figure(figsize=IMAGE_SIZE)
    title = ['Input image', 'True mask', 'Predicted mask']
    for i in range(len(display_list)):
        plt.subplot(1, len(display_list), i + 1)
        plt.title(title[i])
        plt.imshow(tf.keras.preprocessing.image.array_to_img(display_list[i]), cmap='gray')
    plt.show()


def show_dataset(img_data, mask_data, height, width, num=1):
    for i in range(0, num):
        image = img_data[i].reshape(height, width, 1)
        mask = mask_data[i].reshape(height, width, 1)
        display([image, mask])


def show_prediction(model, configuration, img_data, sagittal_data, axial_data, coronal_data, body_surf_data, mask_data, height, width, num=1):
    for i in range(0, num):
        image = img_data[i].reshape(height, width, 1)
        sagittal = sagittal_data[i].reshape(height, width, 1)
        axial = axial_data[i].reshape(height, width, 1)
        coronal = coronal_data[i].reshape(height, width, 1)
        body_surf = body_surf_data[i].reshape(height, width, 1)
        mask = mask_data[i].reshape(height, width, 1)

        features_count = 5


        x = np.zeros([1, height, width, features_count])
        x[0, :, :, 0] = image[:, :, 0]
        x[0, :, :, 1] = sagittal[:, :, 0]
        x[0, :, :, 2] = axial[:, :, 0]
        x[0, :, :, 3] = coronal[:, :, 0]
        x[0, :, :, 4] = body_surf[:, :, 0]
        pred_mask = model.predict(x).reshape(height, width, 1)
        display([image, mask, pred_mask])


def show_prediction_thresholded(model, configuration, img_data, sagittal_data, axial_data, coronal_data, body_surf_data, mask_data, height, width, num=1):
    for i in range(0, num):
        image = img_data[i].reshape(height, width, 1)
        sagittal = sagittal_data[i].reshape(height, width, 1)
        axial = axial_data[i].reshape(height, width, 1)
        coronal = coronal_data[i].reshape(height, width, 1)
        body_surf = body_surf_data[i].reshape(height, width, 1)
        mask = mask_data[i].reshape(height, width, 1)
        features_count = 5
        x = np.zeros([1, height, width, features_count])
        x[0, :, :, 0] = image[:, :, 0]
        x[0, :, :, 1] = sagittal[:, :, 0]
        x[0, :, :, 2] = axial[:, :, 0]
        x[0, :, :, 3] = coronal[:, :, 0]
        x[0, :, :, 4] = body_surf[:, :, 0]
        pred_mask = model.predict(x).reshape(height, width, 1) > 0.5
        display([image, mask, pred_mask])
