# %%


# %%

from __future__ import print_function

import copy

from loguru import logger
import io3d
import io3d.datasets
import sed3
import numpy as np
import matplotlib.pyplot as plt

import matplotlib.pyplot as plt
from pathlib import Path
import bodynavigation
import exsu

import sys
import os

import tensorflow as tf
import os
from skimage.transform import resize
from skimage.io import imsave
import numpy as np
from skimage.segmentation import mark_boundaries
import tensorflow.keras
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, concatenate, Conv2D, MaxPooling2D, Conv2DTranspose
from tensorflow.keras.optimizers import Adam, SGD
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras import backend as K
from tensorflow.keras.callbacks import History
from skimage.exposure import rescale_intensity
from skimage import io
# from data import load_train_data, load_test_data
from sklearn.utils import class_weight
from typing import Optional
from numbers import Number
import datetime
logger.enable("io3d")
logger.disable("io3d")

from unet_keras_tools import window, create_train_data, UNetTrainer, save_segmentations, DataGenerator, read_config

# %%

print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))

print(sys.version_info)
print(sys.executable)
print(os.getcwd())

# %%

def run_experiment(
        organ_label = "rightkidney",
        nb_channels=1,
        epochs=20,
        dim=(256,256),
        experiment_label = "slides_bodynavigation_{organ_label}_{nb_channels}_{datetime}",
        dataset_json='unet_keras_rightkidney.json',
        show=False,
        first_channel=0
):

    dtstr = datetime.datetime.now().strftime("%Y%m%d_%H%M")
    experiment_label = experiment_label.format(organ_label=organ_label, nb_channels=nb_channels, datetime=dtstr)
    rdir = io3d.joinp(f"medical/processed/bodynavigation_unet_keras/report_{experiment_label}_{dtstr}")
    report = exsu.Report(outputdir=rdir, show=show, additional_spreadsheet_fn="data.xlsx")
    reporti=0
    # %%

    datap1 = io3d.datasets.read_dataset("3Dircadb1", "data3d", 1)
    # datap1 = io3d.datasets.read_dataset("sliver07", "data3d", 1)
    data3d = datap1["data3d"]
    sed3.show_slices(data3d, shape=[2, 3], show=show)
    report.savefig(f"{reporti:03d}.png")
    reporti += 1
    # %%

    datap_mask = io3d.datasets.read_dataset("3Dircadb1", organ_label, 1)
    data3d_mask = datap_mask["data3d"]
    sed3.show_slices(data3d_mask, shape=[2, 3], show=show)
    report.savefig(f"{reporti:03d}.png")
    reporti += 1
    # plt.figure()

    # %% md

    ## windowing

    # %%


    data3dw = window(data3d, center=40, width=400)
    # fix, axs = plt.subplots(1,2)
    # axs[]

    # plt.imshow(data3d[30, :, :], cmap='gray')
    # plt.colorbar()
    #
    # plt.figure()
    #
    # plt.imshow(data3dw[30, :, :], cmap='gray')
    # plt.colorbar()
    #
    # %% md

    ## bodynavigation

    # %%

    bn = bodynavigation.body_navigation.BodyNavigation(datap1["data3d"], voxelsize_mm=datap1["voxelsize_mm"])
    dst = bn.dist_to_sagittal()
    plt.imshow(data3d[30, :, :], cmap="gray")
    plt.contour(dst[30, :, :] > 0)
    report.savefig(f"{reporti:03d}.png")
    reporti += 1


    # %%



    # %%

    a = np.asarray([np.stack([data3d[0, :, :], data3d[1, :, :]], axis=2)])
    a.shape


    # %%



    # %%
    skip_if_exists = True

    # create_train_data(
    #     "train",
    #     datasets={
    #         # "3Dircadb1": {"start":1, "stop":5},
    #         "3Dircadb1": {"start":1, "stop":16},
    # #             "sliver07": {"start":1, "stop":16}
    #     },
    #     experiment_label="",
    #     organ_label=organ_label,
    #     skip_if_exists=skip_if_exists
    # )
    # create_train_data(
    #     "test",
    #     datasets={
    #         "3Dircadb1": {"start":16, "stop":21},
    # #             "sliver07": {"start":16, "stop":20}
    #     },
    #     experiment_label="",
    #     organ_label = organ_label,
    #     skip_if_exists=skip_if_exists
    # )



    # %%


    # %%


    # %% md

    # CNN

    # %% md

    # conda install -c conda-forge keras-applications

    # %%


    # %%



    # %%

    data_oh = tf.one_hot(datap_mask['data3d'], 2)
    print(data_oh.shape)
    # print(data_oh)
    sed3.show_slices(data_oh.numpy()[:, :, :, 0].squeeze(), shape=[2, 3], show=show)
    report.savefig(f"{reporti:03d}.png")
    reporti += 1



    # %%


    K.set_image_data_format('channels_last')  # TF dimension ordering in this code

    img_rows = int(512 / 2)
    img_cols = int(512 / 2)


    # We divide here the number of rows and columns by two because we undersample our data (We take one pixel over two)


    # %%

    # wbc = weighted_binary_crossentropy(0.5, 0.5)
    # u = wbc(np.array([1,1,0,1], dtype=np.float32).reshape(4,1,1,1), np.array([1,1,0,1], dtype=np.float32).reshape(4,1,1,1))


    # %%



    #     return imgs_train, imgs_mask_train

    # %%

    f"__{10:04}.png"

    # %%

    # weights_dct

    # %%

    params = {
        'organ_label': organ_label,
        'dim': dim,
        'batch_size': 32,
        # 'n_classes': 6,
        'n_channels': nb_channels,
        'first_channel' : first_channel,
        'shuffle': True}
    params2 = copy.copy((params))
    params2['shuffle'] = False

    # train_dataset_items = [
    #     {"data_set": "3Dircadb1", "id": 1},
    #     {"data_set": "3Dircadb1", "id": 5},
    #     {"data_set": "3Dircadb1", "id": 6},
    # ]
    # validation_dataset_items = [
    #     {"data_set": "3Dircadb1", "id": 7},
    #     {"data_set": "3Dircadb1", "id": 11},
    # ]
    #
    # test_dataset_items = [
    #     {"data_set": "3Dircadb1", "id": 20},
    # ]
    cfg = read_config(Path(dataset_json).absolute())

    train_dataset_items = cfg["train_dataset_items"]
    validation_dataset_items = cfg["validation_dataset_items"]
    test_dataset_items = cfg["test_dataset_items"]
    training_generator = DataGenerator(train_dataset_items, **params)
    validation_generator = DataGenerator(validation_dataset_items, **params2)
    test_generator = DataGenerator(test_dataset_items, **params2)
    unt = UNetTrainer(nb_channels, img_rows, img_cols, experiment_label, organ_label=organ_label,
                      output_dir=io3d.joinp(r"medical\processed\bodynavigation_unet_keras")

                      )
    # unt.calculate_weights(training_generator)

    # unt.calculate_weights(training_generator)
    history = unt.train_and_predict(
        training_generator, validation_generator, epochs=epochs)
    plt.plot(history.history['dice_coef'])
    plt.plot(history.history['val_dice_coef'])
    plt.title('Model dice coeff')
    plt.ylabel('Dice coeff')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    report.savefig("Dice coef")
    print("predict test data")
    # test_generator.shuffle=False
    # test_generator.on_epoch_end()

    unt.predict_test_data(test_generator, prefix="test_")
    unt.predict_test_data(validation_generator, prefix="validation_")
    # history = train_and_predict(continue_training=True, epochs=10)

    # %%

    ## Recalculate predictions for train dataset
    # imgs_train, imgs_mask_train = load_test_data()

    # imgs_train = preprocess(imgs_train)
    # imgs_mask_train = preprocess(imgs_mask_train)

    # imgs_train = imgs_train.astype('float32')
    # mean = np.mean(imgs_train)  # mean for data centering
    # std = np.std(imgs_train)  # std for data normalization
    # unt.predict_test_data(test_generator)

    # %% md

    # Try one image

    # %%

    # imgs_train, imgs_mask_train = get_dataset_loaders("train", organ_label)

    # imgs_train = unt.preprocess(imgs_train)
    # imgs_mask_train = unt.preprocess(imgs_mask_train)

    # imgs_train = imgs_train.astype('float32')
    # mean = np.mean(imgs_train)  # mean for data centering
    # std = np.std(imgs_train)  # std for data normalization

    # imgs_train -= mean
    # imgs_train /= std
    # Normalization of the train set

    # imgs_mask_train = imgs_mask_train.astype('float32')

    # print(f"Number of frames={imgs_train.shape[0]}")

    # %%
    # ---------------------------------------------------------

    unt = UNetTrainer(nb_channels, img_rows, img_cols, experiment_label, organ_label)
    model = unt.get_unet()

    model.load_weights(f'weights_{experiment_label}.h5')
    unt.calculate_weights(training_generator)

    logger.debug('-' * 30)
    logger.debug('Predicting masks on test data...')
    logger.debug('-' * 30)
    imgs_mask_train_pred = model.predict(test_generator, verbose=1)

    # %%

    import scipy.stats

    logger.debug(scipy.stats.describe(imgs_mask_train_pred.flatten()))

    # %%

    # i = 86

    # %%

    # plt.imshow(imgs_train[i, :, :, 0], cmap='gray')
    # plt.colorbar()
    # report.savefig(f"img_{i}.png")
    #
    # # %%
    #
    # plt.imshow(imgs_mask_train_pred[i, :, :], cmap='gray')
    # plt.colorbar()
    # report.savefig(f"mask_pred_{i}.png")
    #
    # # %%
    #
    # plt.imshow(imgs_mask_train[i, :, :], cmap='gray')
    # plt.colorbar()
    # report.savefig(f"mask_true_{i}.png")
    #
    # # %%
    #
    # logger.debug(tf.keras.losses.binary_crossentropy(imgs_mask_train[i, :, :].flatten(), imgs_mask_train_pred[i, :, :].flatten()))
    #
    # # %%
    #
    # logger.debug(tf.keras.losses.binary_crossentropy(imgs_mask_train.flatten(), imgs_mask_train_pred.flatten()))
    #
    # # %%
    #
    # save_segmentations(imgs_train[:, :, :,  0], imgs_mask_train_pred[..., 0], f"preds_test/{experiment_label}")
    #
    # # %%
    #
    # y_train = (imgs_mask_train > 0).astype(np.float32)
    # weights = class_weight.compute_class_weight('balanced', np.unique(y_train.flatten()), y_train.flatten())
    # # y_train.shape
    # # imgs_train.shape
    # # y_train.dtype
    # # print(np.unique(imgs_mask_train > 0))
    #
    # # plt.imshow(imgs_mask_train[150,:,:] > 0, interpolation='None')
    # logger.debug(weights)
    #
    # # %%
    #
    #



# run_experiment(
#     # organ_label="rightkidney",
#     organ_label="liver",
#     nb_channels=5,
#     dim=(256, 256),
#     epochs=30,
#     experiment_label="slides_bodynavigation_{organ_label}_{nb_channels}_{datetime}",
#     show=False,
#     dataset_json="../experiments/experiment_bodynav.json",
#     # dataset_json = "../experiments/experiment_bodynav2.json",
#     # dataset_json = 'unet_keras_rightkidney.json'
#     first_channel=0
# )
# run_experiment(
#     # organ_label="rightkidney",
#     organ_label="liver",
#     nb_channels=4,
#     dim=(256, 256),
#     epochs=30,
#     experiment_label="slides_bodynavigation_{organ_label}_{nb_channels}_{datetime}",
#     show=False,
#     dataset_json="../experiments/experiment_bodynav.json",
#     # dataset_json = "../experiments/experiment_bodynav2.json",
#     # dataset_json = 'unet_keras_rightkidney.json'
#     first_channel=1
# )
run_experiment(
    # organ_label="rightkidney",
    organ_label="liver",
    nb_channels=1,
    dim=(256, 256),
    epochs=30,
    experiment_label="slides_bodynavigation_{organ_label}_{nb_channels}_{datetime}",
    show=False,
    # dataset_json="../experiments/experiment_bodynav.json"
    dataset_json = "../experiments/experiment_bodynav2.json",
    first_channel=0
    # dataset_json = 'unet_keras_rightkidney.json'
)
