import pytest
from loguru import logger
from pathlib import Path
import numpy as np

from . import unet_keras_tools


def test_read_dataset_given_dataset_items():
    logger.debug(Path(".").absolute())
    dataset_items = [
        {"data_set": "3Dircadb1", "id": 1},
        {"data_set": "3Dircadb1", "id": 3},
        {"data_set": "3Dircadb1", "id": 4},
        {"data_set": "3Dircadb1", "id": 5},
        {"data_set": "3Dircadb1", "id": 6},
    ]
    dl0, dl1 = unet_keras_tools.get_dataset_loaders(dataset_items, organ_label="rightkidney")
    pass

def test_read_dataset():
    cfg = unet_keras_tools.read_config(Path('experiments/experiment_000.json.'))
    dataset_items = cfg["train_dataset_items"]
    dl0, dl1 = unet_keras_tools.get_dataset_loaders(dataset_items, organ_label="rightkidney")
    uu = next(dl0)
    print(uu)
    logger.debug(uu)


def test_read_dataset_loader():
    dataset_items = [
        {"data_set": "3Dircadb1", "id": 1},
        # {"data_set": "3Dircadb1", "id": 3},
        # {"data_set": "3Dircadb1", "id": 4},
        # {"data_set": "3Dircadb1", "id": 5},
        # {"data_set": "3Dircadb1", "id": 6},
    ]
    gen = unet_keras_tools.DataGenerator(
        organ_label="rightkidney", dataset_items=dataset_items,
    )
    # l = len(gen)
    Xarr, yarr = unet_keras_tools.get_ndarray_from_generator(gen)
    assert Xarr.shape[1] == 512
    assert Xarr.shape[2] == 512


def test_training():
    train_dataset_items = [
        {"data_set": "3Dircadb1", "id": 1},
        # {"data_set": "3Dircadb1", "id": 3},
        # {"data_set": "3Dircadb1", "id": 4},
        # {"data_set": "3Dircadb1", "id": 5},
        # {"data_set": "3Dircadb1", "id": 6},
    ]
    valid_dataset_items = [
        # {"data_set": "3Dircadb1", "id": 1},
        {"data_set": "3Dircadb1", "id": 1},
        # {"data_set": "3Dircadb1", "id": 4},
        # {"data_set": "3Dircadb1", "id": 5},
        # {"data_set": "3Dircadb1", "id": 6},
    ]
    nb_channels = 4
    params = {
        'organ_label': 'rightkidney',
        'dim': (256, 256),
        'batch_size': 32,
        # 'n_classes': 6,
        'n_channels': nb_channels,
        'shuffle': True}
    train_gen = unet_keras_tools.DataGenerator(
        dataset_items=train_dataset_items, **params
    )
    valid_gen = unet_keras_tools.DataGenerator(
        dataset_items=valid_dataset_items, **params
    )

    img_rows = params['dim'][0]
    img_cols = params['dim'][0]

    experiment_label = "pokus"
    unt = unet_keras_tools.UNetTrainer(nb_channels, img_rows, img_cols, experiment_label=experiment_label, organ_label=params['organ_label'])
    model = unt.get_unet()
    unt.train_and_predict(train_gen, valid_gen, epochs=6)

    # model.load_weights(f'weights_{experiment_label}.h5')
    # np.array(gen)


def test_predict():
    train_dataset_items = [
        {"data_set": "3Dircadb1", "id": 1},
        # {"data_set": "3Dircadb1", "id": 3},
        # {"data_set": "3Dircadb1", "id": 4},
        # {"data_set": "3Dircadb1", "id": 5},
        # {"data_set": "3Dircadb1", "id": 6},
    ]


    valid_dataset_items = [
        # {"data_set": "3Dircadb1", "id": 1},
        # {"data_set": "3Dircadb1", "id": 3},
        # {"data_set": "3Dircadb1", "id": 4},
        {"data_set": "3Dircadb1", "id": 5},
        # {"data_set": "3Dircadb1", "id": 6},
    ]
    nb_channels = 2
    nb_channels = 4
    params = {
        'organ_label': 'rightkidney',
        'dim': (256, 256),
        'batch_size': 32,
        # 'n_classes': 6,
        'n_channels': nb_channels,
        'shuffle': True}
    train_gen = unet_keras_tools.DataGenerator(
        dataset_items=train_dataset_items, **params
    )
    valid_gen = unet_keras_tools.DataGenerator(
        dataset_items=valid_dataset_items, **params
    )

    img_rows = params['dim'][0]
    img_cols = params['dim'][0]

    experiment_label = "pokus"
    unt = unet_keras_tools.UNetTrainer(nb_channels, img_rows, img_cols, experiment_label, params['organ_label'])
    model = unt.get_unet()
    unt.predict_test_data(valid_gen)
    # unt.train_and_predict(train_gen, valid_gen, epochs=1)


def test_calculate_weights():
    nb_channels = 2
    train_dataset_items = [
        {"data_set": "3Dircadb1", "id": 1},
        {"data_set": "3Dircadb1", "id": 3},
        {"data_set": "3Dircadb1", "id": 4},
        {"data_set": "3Dircadb1", "id": 5},
        {"data_set": "3Dircadb1", "id": 6},
    ]
    params = {
        'organ_label': 'liver',
        'dim': (256, 256),
        'batch_size': 32,
        # 'n_classes': 6,
        'n_channels': nb_channels,
        'shuffle': True}
    train_gen = unet_keras_tools.DataGenerator(
        dataset_items=train_dataset_items, **params
    )
    weights = unet_keras_tools.calculate_weights(train_gen)


def test_save_images():
    nb_channels = 2

    train_dataset_items = [
        {"data_set": "3Dircadb1", "id": 1},
        # {"data_set": "3Dircadb1", "id": 3},
        # {"data_set": "3Dircadb1", "id": 4},
        {"data_set": "3Dircadb1", "id": 5},
        # {"data_set": "3Dircadb1", "id": 6},
    ]
    params = {
        'organ_label': 'rightkidney',
        'dim': (256, 256),
        'batch_size': 32,
        # 'n_classes': 6,
        'n_channels': nb_channels,
        'shuffle': False}
    train_gen = unet_keras_tools.DataGenerator(
        dataset_items=train_dataset_items, **params
    )
    X, y = unet_keras_tools.get_ndarray_from_generator(train_gen, get_channel_number=0)
    logger.debug(X.shape)
    logger.debug(y.shape)
    unet_keras_tools.save_segmentations(X, y, pred_dir="test_preds/")


def test_decompose_dataset():
    nb_channels = 2


    train_dataset_items = [
        {"data_set": "3Dircadb1", "id": 1},
        {"data_set": "3Dircadb1", "id": 3},
        {"data_set": "3Dircadb1", "id": 4},
        {"data_set": "3Dircadb1", "id": 5},
        {"data_set": "3Dircadb1", "id": 6},
    ]
    params = {
        'organ_label': 'liver',
        'dim': (256, 256),
        'batch_size': 32,
        # 'n_classes': 6,
        'n_channels': nb_channels,
        'shuffle': True}
    train_gen = unet_keras_tools.DataGenerator(
        dataset_items=train_dataset_items, **params
    )
    fl = map(train_gen.file_list_x, str)
    dct = set()
    for fnp in train_gen.file_list_x:
        dataset, id, i = str(fnp).split("_")
        dct.add((dataset, id))


def test_orientation_axcodes():
    import io3d.datasets
    dp10 = io3d.datasets.read_dataset("3Dircadb1", 'data3d', 1, orientation_axcodes='SPL')
    dp11 = io3d.datasets.read_dataset("3Dircadb1", 'data3d', 1, orientation_axcodes='original')

    assert dp10.data3d[100,100,100] == dp11.data3d[100,100,100]

    dp10 = io3d.datasets.read_dataset("sliver07", 'data3d', 1, orientation_axcodes='SPL')
    dp11 = io3d.datasets.read_dataset("sliver07", 'data3d', 1, orientation_axcodes='original')

    assert dp10.data3d[100,100,100] != dp11.data3d[100,100,100], "sliver are stored reversely"


def test_check_features():
    from matplotlib import pyplot as plt
    import itertools
    import io3d
    nb_channels = 5
    for dataset, ind in itertools.product(["3Dircadb1","sliver07"], list(range(1, 21))):
        train_dataset_items = [
            {"data_set": dataset, "id": ind},
        ]
        params = {
            'organ_label': 'liver',
            # 'dim': (256, 256),
            'dim': (512, 512),
            'batch_size': 32,
            # 'n_classes': 6,
            'n_channels': nb_channels,
            'shuffle': False}
        train_gen = unet_keras_tools.DataGenerator(
            dataset_items=train_dataset_items, **params
        )
        features = []
        mask = []
        for features_i, mask_i in train_gen:
            features.append(features_i)
            mask.append(mask_i)

        features = np.concatenate(features, axis=0)
        mask = np.concatenate(mask, axis=0)
        # plt.figure()
        fig, axs = plt.subplots(2, features.shape[3], sharex=True, sharey="row", constrained_layout=True,
                                figsize=(10,5)
                                )
        fig.tight_layout()
        shalf0 = np.int(features.shape[0]/2)
        shalf1 = np.int(features.shape[1]/2)
        shalf2 = np.int(features.shape[2]/2)
        shalf3 = np.int(features.shape[3]/2)
        titles = ["Density", "Sagittal", "Coronal", "Axial", "Surface"
                  # bn.dist_to_sagittal(),
                  # bn.dist_coronal(),
                  # bn.dist_to_diaphragm_axial(),
                  # bn.dist_to_surface(),
                  ]
        for i in range(features.shape[3]):
            axs[0][i].axis("off")
            axs[0][i].imshow(features[shalf0,:,:,i], cmap='gray')
            axs[0][i].contour(mask[shalf0,:,:])
            if i > 0:
                axs[0][i].contour(features[shalf0,:,:,i]>0, colors=["r"])
            axs[1][i].axis("off")
            axs[1][i].imshow(features[:,shalf1,:,i], cmap='gray', aspect=2.)
            axs[1][i].contour(mask[:,shalf1,:])
            if i > 0:
                axs[1][i].contour(features[:,shalf1,:,i]>0, colors=["r"])
            axs[0][i].set_title(titles[i])

        # axs[0][0].ylabel("axial")
        pth = io3d.joinp(f'medical/processed/bodynavigation_unet_keras/check_features/features_{dataset}_{ind:02d}.pdf')
        pth.parent.mkdir(exist_ok=True, parents=True)
        # fig.tight_layout(pad=0.05)
        plt.subplots_adjust(wspace=0.01, hspace=0.01)
        # gs.update(wspace=0.1, hspace=0.1, left=0.1, right=0.4, bottom=0.1, top=0.9)
        # plt.subplots_adjust(wspace=0.1, hspace=0.1, left=0.1, right=0.4, bottom=0.1, top=0.1)
        # plt.subplots_adjust(bottom=0.4, top=0.6, hspace=-0.1)
        # plt.subplots_adjust(hspace=-0.1)
        # fig.subplots_adjust(wspace=0, hspace=0)
        plt.savefig(pth, dpi=400, bbox="tight",
                    pad_inches = 0
                    )
        pth = io3d.joinp(f'medical/processed/bodynavigation_unet_keras/check_features/features_{dataset}_{ind:02d}.png')
        plt.savefig(pth, dpi=300, bbox="tight",
                    pad_inches=0
                    )







