from __future__ import print_function
from loguru import logger
import io3d
import io3d.datasets
import sed3
import numpy as np
import matplotlib.pyplot as plt

import matplotlib.pyplot as plt
from pathlib import Path
import bodynavigation
import exsu

import sys
import os

import tensorflow as tf
import os
from skimage.transform import resize
from skimage.io import imsave
import numpy as np
from skimage.segmentation import mark_boundaries
import tensorflow.keras
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, concatenate, Conv2D, MaxPooling2D, Conv2DTranspose
from tensorflow.keras.optimizers import Adam, SGD
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras import backend as K
from tensorflow.keras.callbacks import History
from skimage.exposure import rescale_intensity
from skimage import io
# from data import load_train_data, load_test_data
from sklearn.utils import class_weight
from typing import Optional
from numbers import Number
import datetime
import exsu
logger.enable("io3d")
logger.disable("io3d")

from unet_keras_tools import window, create_train_data, UNetTrainer, save_segmentations, DataGenerator, read_config

# %%

print("Num GPUs Available: ", len(tf.config.experimental.list_physical_devices('GPU')))

print(sys.version_info)
print(sys.executable)
print(os.getcwd())


# %%

def do_evaluation(
    # organ_label = "rightkidney"
    nb_channels=5,
    fn_weights = "weights_slides_bodynavigation_liver_5_20210629_1149_.05.h5",
    # fn_weights = "weights_slides_bodynavigation_liver_1_20210629_2346_.05.h5",
    organ_label = "liver",
    experiment_label = "slides_bodynavigation_{organ_label}_{nb_channels}_1",
    show=False,
    epoch = 5,
    ):
    experiment_label = experiment_label.format(organ_label=organ_label, nb_channels=nb_channels)


    report = exsu.Report(io3d.joinp("medical/processed/bodynavigation_unet_keras/report"),
                         additional_spreadsheet_fn=io3d.joinp("medical/processed/bodynavigation_unet_keras/report.xlsx")
                         )

    params = {
        'organ_label': organ_label,
        'dim': (256, 256),
        'batch_size': 32,
        # 'n_classes': 6,
        'n_channels': nb_channels,
        'shuffle': False}

    # train_dataset_items = [
    #     {"data_set": "3Dircadb1", "id": 1},
    #     {"data_set": "3Dircadb1", "id": 5},
    #     {"data_set": "3Dircadb1", "id": 6},
    # ]
    # validation_dataset_items = [
    #     {"data_set": "3Dircadb1", "id": 7},
    #     {"data_set": "3Dircadb1", "id": 11},
    # ]
    #
    # test_dataset_items = [
    #     {"data_set": "3Dircadb1", "id": 20},
    # ]
    cfg = read_config(Path('../experiments/experiment_bodynav.json').absolute())

    train_dataset_items = cfg["train_dataset_items"]
    validation_dataset_items = cfg["validation_dataset_items"]
    test_dataset_items = cfg["test_dataset_items"]
    training_generator = DataGenerator(train_dataset_items, **params)
    validation_generator = DataGenerator(validation_dataset_items, **params)
    test_generator = DataGenerator(test_dataset_items, **params)
    unt = UNetTrainer(nb_channels, params['dim'][0],params['dim'][0], experiment_label, organ_label=organ_label)
    # unt.calculate_weights(training_generator)
    # unt.predict_test_data(test_generator, prefix="test_", fn_weights=fn_weights)
    # unt.predict_test_data(validation_generator, prefix="validation_", epoch=epoch)
    report.set_persistent_cols()
    datetime.datetime.now().strftime("%Y%m%d_%H%M")

    # olist = []
    report.set_persistent_cols(
        {"experiment_label": unt.experiment_label, "epoch": epoch, "datetime":datetime, "fn_weights":fn_weights})

    unt.evaluate(test_generator, report, prefix="test_", fn_weights=fn_weights)
    unt.evaluate(validation_generator, report, prefix="validation_", fn_weights=fn_weights)

do_evaluation(
        # organ_label = "rightkidney"
        nb_channels=5,
        fn_weights = "weights_slides_bodynavigation_liver_5_20210629_1149_.05.h5",
        # fn_weights = "weights_slides_bodynavigation_liver_1_20210629_2346_.05.h5",
        organ_label = "liver",
        experiment_label = "slides_bodynavigation_{organ_label}_{nb_channels}_1",
        show=False,
        epoch = 5,
)
do_evaluation(
    # organ_label = "rightkidney"
    nb_channels=1,
    # fn_weights = "weights_slides_bodynavigation_liver_5_20210629_1149_.05.h5",
    fn_weights = "weights_slides_bodynavigation_liver_1_20210629_2346_.05.h5",
    organ_label = "liver",
    experiment_label = "slides_bodynavigation_{organ_label}_{nb_channels}_1",
    show=False,
    epoch = 5,
)
